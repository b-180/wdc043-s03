import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        ArrayList<String> topGames = new ArrayList<String>();

        HashMap<String, Integer> games = new HashMap<>()
        {{
            put("Mario Odyssey", 50);
            put("Super Smash Bros. Ultimate", 20);
            put("Luigi's Mansion 3", 15);
            put("Pokemon Sword", 30);
            put("Pokemon Shield", 100);
        }};
        games.forEach((key, value)->{
            System.out.printf("%s has %d stocks left.\n",key,value);

        });
        games.forEach((key, value)->{
            if (value <= 30) {
                topGames.add(key);
                System.out.printf("%s has been added to top games list!\n", key);
            }

        });
        System.out.println("Our Shop's top games:");
        System.out.println(topGames);



    }
}
//
//    HashMap<String, String> userRoles = new HashMap<>();
//
////Add new fields in the hashmap:
////hashMapName.put(<item>);
//        userRoles.put("Anna", "Admin");
//                userRoles.put("Alice", "User");
//                userRoles.put("Boy", "Admin");
//                System.out.println(userRoles);
//
//                //Retrieve values by fields
//                //hasMapName.get("field");
//                System.out.println(userRoles.get("Anna"));
//
//                //Remove an element
//                //hashMapName.remove("field");
//                System.out.println(userRoles.remove("Boy"));
//                System.out.println(userRoles);
//
//                //Retrieve hashMap keys
//                //hashMapName.keySet();
//                System.out.println(userRoles.keySet());