import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void whileDemo() {
        int a = 1;
        while (a < 5) {
            System.out.format("While Loop Counter %d\n", a++);
        }

        Scanner scan = new Scanner(System.in).useDelimiter("\\n");
        String name = "";

        try {
            while (name.isBlank()) {
                System.out.println("What's your name?");
                name = scan.nextLine();

                if (!name.isBlank()) {
                    System.out.format("Hi %s!", name);
                }
            }
        } catch (Exception e) {
            System.out.println("Invalid Input");
            e.printStackTrace();
        }
    }

    public static void loopDemo(){
        for (int i = 0; i < 5; i++) {
            System.out.format("Count: %d\n", i);
        }

        int[] intArray = {100,200,300,400,500};
        for (int i = 0; i < intArray.length; i++) {
            System.out.printf("Item Index %d\n", intArray[i]);
        }

        String classroom[][] = new String[3][3];
        classroom[0][0] = "Rayquaza";
        classroom[0][1] = "Kyogre";
        classroom[0][2] = "Groudon";

        classroom[1][0] = "Sora";
        classroom[1][1] = "Goofy";
        classroom[1][2] = "Donald";

        classroom[2][0] = "Harry";
        classroom[2][1] = "Ron";
        classroom[2][2] = "Hermione";

        //1st row is in [] because it contains an array
        for (String[] row:classroom) {
            //2nd row has no [] since it contains strings only
            for (String student:row) {
                System.out.println(row);
            }
        }

        //Nested For Loop
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                System.out.println(classroom[row][col]);
            }
        }


    }
    public static void forEachDemo() {
        //Enhanced For Loop for Java Array/ArrayList
        //In Java, a for-each loop can be used to iterate over the items of an array and arrayList
        //for-each in Java for array and arrayList is also called enhanced for loop.
        //member is a parameter which will represent each item in the given array

        String[] members = {"Eugene","Vincent","Dennis","Alfred"};
        for (String member:members) {
            System.out.println(member);
        }
    }

    public static void hashMapForEachDemo() {
        /*
            HashMap forEach
            HashMap has a method for iteration each field

         */
        String[] members = {"Eugene","Vincent","Dennis","Alfred"};
        for (String member:members) {
            System.out.println(member);
        }

        HashMap<String, String> techniques = new HashMap<>();
        techniques.put(members[0], "Spirit Gun");
        techniques.put(members[1], "Black Dragon");
        techniques.put(members[2], "Rose Whip");
        techniques.put(members[3], "Spirit Sword");
        System.out.println(techniques);

        techniques.forEach((key, value)->{
            System.out.printf("Member %s uses %s.\n",key, value);
        });
    }

    public static void main(String[] args) {
            //whileDemo();
            //loopDemo();
            //forEachDemo();
            hashMapForEachDemo();
        }
}
